//
//  EditBasicInfoTableViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/4/26.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "EditBasicInfoTableViewController.h"
#import "BasicInfo2TableViewCell.h"
#import "MHDatePicker.h"
#import "MMPickerView.h"


@interface EditBasicInfoTableViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) MHDatePicker *selectDatePicker;
@property (nonatomic, strong) NSArray *stringsArray;
@property (nonatomic, strong) NSString * selectedString;
@end

@implementation EditBasicInfoTableViewController

static NSString *CellIdentifier2 = @"Celltext";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveInfo)];
    
    self.dataArray1 = [[NSMutableArray alloc] initWithObjects:@"用户名", @"平台账号", @"手机号",@"性别",@"生日", nil];
    self.valueArray1 = [[NSMutableArray alloc] initWithObjects:@"用户名", @"平台账号", @"手机号",@"性别",@"生日", nil];
    self.dataArray2 = [[NSMutableArray alloc] initWithObjects:@"职业", @"公司", @"所在地", @"简介", nil];
    self.valueArray2 = [[NSMutableArray alloc] initWithObjects:@"职业", @"公司", @"所在地", @"简介", nil];
    _stringsArray = @[@"男", @"女"];
    _selectedString = [_stringsArray objectAtIndex:0];
}

-(void)saveInfo
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0)
    {
        return 0;
    }
    else
    {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 50;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    if(section==0)
    {
        return [self.dataArray1 count];
    }
    else if(section==1)
    {
        return [self.dataArray2 count];
    }
    else
    {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    if(indexPath.section==0)
    {
        BasicInfo2TableViewCell *cell = (BasicInfo2TableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if(cell == nil)
        {
            
            cell = [[BasicInfo2TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
            
        }
        NSString* val = @"";
        if(indexPath.row==0)
        {
            val = @"0";
        }
        else if(indexPath.row==1)
        {
            val = @"1";
        }
        else if(indexPath.row==2)
        {
            val = @"2";
        }
        else if(indexPath.row==3)
        {
            val = @"3";
        }
        else if(indexPath.row==4)
        {
            val = @"4";

            //cell.nameLabel.inputView = picker;
        }
        //[cell.nameLabel setText: [self.dataArray1 objectAtIndex:indexPath.row]];
        [cell setContentWithModel:[self.dataArray1 objectAtIndex:indexPath.row] v:[self.valueArray1 objectAtIndex:indexPath.row]];
        return cell;
        
    }
    else if(indexPath.section==1)
    {
        BasicInfo2TableViewCell *cell = (BasicInfo2TableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if(cell == nil)
        {
            
            cell = [[BasicInfo2TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
            
        }
        NSString* val = @"";
        if(indexPath.row==0)
        {
            val = @"0";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else if(indexPath.row==1)
        {
            val = @"1";
        }
        else if(indexPath.row==2)
        {
            val = @"2";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else if(indexPath.row==3)
        {
            val = @"3";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        //cell.backgroundColor=[UIColor colorWithRed:235.0/255 green:235.0/255 blue:243.0/255 alpha:1.0];
        [cell setContentWithModel:[self.dataArray2 objectAtIndex:indexPath.row] v:[self.valueArray2 objectAtIndex:indexPath.row]];
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:203.0/255 green:203.0/255 blue:203.0/255 alpha:1.0];
        
        //cell.inputView = nil;

        return cell;
    }
    else
    {
        return nil;
    }
    
    // Configure the cell...
    
    
}

- (NSString *)dateStringWithDate:(NSDate *)date DateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    NSString *str = [dateFormatter stringFromDate:date];
    return str ? str : @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section==0&&indexPath.row==4)
    {
    _selectDatePicker = [[MHDatePicker alloc] init];
    _selectDatePicker.isBeforeTime = YES;
    _selectDatePicker.datePickerMode = UIDatePickerModeDate;
    
    __weak typeof(self) weakSelf = self;
    [_selectDatePicker didFinishSelectedDate:^(NSDate *selectedDate) {
        __strong typeof(self) strongSelf = weakSelf;
                //NSString *string = [NSString stringWithFormat:@"%@",[NSDate dateWithTimeInterval:3600*8 sinceDate:selectedDate]];
                //weakSelf.myLabel2.text = string;
        strongSelf.valueArray1[indexPath.row] = [strongSelf dateStringWithDate:selectedDate DateFormat:@"yyyy年MM月dd日"];
        [strongSelf.tableView reloadData];
    }];
    }
    else if(indexPath.section==0&&indexPath.row==3)
    {
        __weak typeof(self) weakSelf = self;
        [MMPickerView showPickerViewInView:self.view
                               withStrings:_stringsArray
                               withOptions:@{MMbackgroundColor: [UIColor whiteColor],
                                             MMtextColor: [UIColor blackColor],
                                             MMtoolbarColor: [UIColor whiteColor],
                                             MMbuttonColor: [UIColor blueColor],
                                             MMfont: [UIFont systemFontOfSize:18],
                                             MMvalueY: @3,
                                             MMselectedObject:_selectedString,
                                             MMtextAlignment:@1}
                                completion:^(NSString *selectedString) {
                                    __strong typeof(self) strongSelf = weakSelf;
                                    strongSelf.valueArray1[indexPath.row]=selectedString;
                                    [strongSelf.tableView reloadData];
                                    //_label.text = selectedString;
                                    //_selectedString = selectedString;
                                }];
    }
    
}

// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the specified item to be editable.
//    return YES;
//}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
