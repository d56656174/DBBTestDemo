//
//  MallTabIconCollectionViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallTabIconCollectionViewCell : UICollectionViewCell
@property(nonatomic,strong) UIImageView *icon;
@property(nonatomic,strong) UILabel *label;
-(void)initSubviews:(UIImage*)image  labeltxt:(NSString*)lab;
@end
