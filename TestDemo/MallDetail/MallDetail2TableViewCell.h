//
//  MallDetail2TableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallDetail2TableViewCell : UITableViewCell

@property(nonatomic,strong) UILabel *uilabel;
@property(nonatomic,strong) UILabel *uivalue;
-(void)initSubviews;
-(void)setLayout;

@end
