//
//  MallDetail2TableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetail2TableViewCell.h"

@implementation MallDetail2TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
        [self initSubviews];
        [self setLayout];
    }
    return self;
}
-(void)initSubviews
{
    //[self setImage:[[UIImageView alloc] init]];
    [self setUilabel:[[UILabel alloc] init]];
    [self setUivalue:[[UILabel alloc] init]];
    
    self.uilabel.font = [UIFont systemFontOfSize:12];
    self.uivalue.font = [UIFont systemFontOfSize:12];
    
    
    [self.contentView addSubview:self.uilabel];
    [self.contentView addSubview:self.uivalue];
    
}


-(void)setLayout
{
    
    
    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"uilable1":self.uilabel,@"uivalue":self.uivalue};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[uilable1]-[uivalue]"] options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-20-[uilable1]"] options:0 metrics:nil views:superviewdict]];
    
    
    
    
}

@end
