//
//  BussinessPromisTableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/23.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "BussinessPromisTableViewCell.h"

@implementation BussinessPromisTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
        [self initSubviews];
        [self setLayout];
    }
    return self;
}
-(void)initSubviews
{
    
    [self setNameLabel1:[[UILabel alloc] init]];
    [self setNameLabel2:[[UILabel alloc] init]];
    
    [self setNameLabel3:[[UILabel alloc] init]];
    [self setNameLabel4:[[UILabel alloc] init]];
    [self setNameLabel5:[[UILabel alloc] init]];
    
    self.nameLabel1.text = @"商家承诺";
    self.nameLabel1.font = [UIFont systemFontOfSize:14];
    self.nameLabel2.text = @"品种";
    self.nameLabel2.font = [UIFont systemFontOfSize:12];
    self.nameLabel3.text = @"成活率";
    self.nameLabel3.font = [UIFont systemFontOfSize:12];
    self.nameLabel4.text = @"出栏时间－－－天";
    self.nameLabel4.font = [UIFont systemFontOfSize:12];
    self.nameLabel5.text = @"出栏重量－－－斤/每只";
    self.nameLabel5.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.nameLabel1];
    [self.contentView addSubview:self.nameLabel2];
    [self.contentView addSubview:self.nameLabel3];
    [self.contentView addSubview:self.nameLabel4];
    [self.contentView addSubview:self.nameLabel5];
    
}


-(void)setLayout
{
    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"nameLabel1":_nameLabel1,@"nameLabel2":_nameLabel2,@"nameLabel3":_nameLabel3,@"nameLabel4":_nameLabel4,@"nameLabel5":_nameLabel5};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-30-[nameLabel1]-20-[nameLabel2]-10-[nameLabel3]-10-[nameLabel4]-10-[nameLabel5]"] options:NSLayoutFormatAlignAllLeft|NSLayoutFormatAlignAllRight metrics:nil views:superviewdict]];

    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[nameLabel1]-8-|"] options:0 metrics:nil views:superviewdict]];
   
    
}


@end
