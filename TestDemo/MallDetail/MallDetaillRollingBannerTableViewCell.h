//
//  MallDetaillRollingBannerTableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/22.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallDetaillRollingBannerTableViewCell : UITableViewCell
@property (strong,nonatomic) NSMutableArray *dataArray1;
-(void)initSubviews;
@end
