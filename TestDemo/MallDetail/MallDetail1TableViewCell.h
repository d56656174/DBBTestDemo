//
//  MallDetail1TableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/28.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallDetail1TableViewCell : UITableViewCell

@property(nonatomic,strong) UILabel *title1;//大肥鹅大肥鹅大肥鹅大肥鹅大肥鹅大肥鹅
@property(nonatomic,strong) UILabel *price;//价格
@property(nonatomic,strong) UILabel *salecount;//当月销量
@property(nonatomic,strong) UILabel *place;//北京
@property(nonatomic,strong) UILabel *name;//大肥鹅
@property(nonatomic,strong) UILabel *outcount;//出货数量
@property(nonatomic,strong) UILabel *outdate;//出货日期

-(void)initSubviews;
-(void)setLayout;
@end
