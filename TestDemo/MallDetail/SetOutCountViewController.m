//
//  SetOutCountViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/31.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "SetOutCountViewController.h"

@interface SetOutCountViewController ()<UITextFieldDelegate>

@end

@implementation SetOutCountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    //self.alphaiView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    self.alphaiView.backgroundColor = [UIColor blackColor];
//    self.alphaiView.alpha = 0.2;
//    [self.view addSubview:self.alphaiView];
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.textField = [[UITextField alloc] init];
    
    [self.view addSubview:self.textField];
    self.textField.placeholder = @"订购数量";
    self.textField.delegate = self;

    
    [self setLayout];
}

-(void)setLayout
{
    
    
    for (UIView *view in self.view.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"textField":self.textField};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[textField]-|"] options:0 metrics:nil views:superviewdict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-20-[textField]"] options:0 metrics:nil views:superviewdict]];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];    //主要是[receiver resignFirstResponder]在哪调用就能把receiver对应的键盘往下收
    [self.detegate setOutCount:textField.text];
    //self.valueArray1[textField.tag]=textField.text;
    //NSLog(@"%@",self.valueArray1[textField.tag]);
    //[self.tableView reloadData];
//    [self dismissViewControllerAnimated:YES completion:^{
//        NSLog(@"移除");
//    }];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
