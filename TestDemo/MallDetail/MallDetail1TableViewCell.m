//
//  MallDetail1TableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/28.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetail1TableViewCell.h"

@implementation MallDetail1TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
        [self initSubviews];
        [self setLayout];
    }
    return self;
}
-(void)initSubviews
{
    //[self setImage:[[UIImageView alloc] init]];
    [self setTitle1:[[UILabel alloc] init]];
    [self setPrice:[[UILabel alloc] init]];
    [self setSalecount:[[UILabel alloc] init]];
    [self setPlace:[[UILabel alloc] init]];
    [self setName:[[UILabel alloc] init]];
    [self setOutcount:[[UILabel alloc] init]];
    [self setOutdate:[[UILabel alloc] init]];

    self.title1.font = [UIFont systemFontOfSize:12];
    self.price.font = [UIFont systemFontOfSize:10];
    self.salecount.font = [UIFont systemFontOfSize:10];
    self.place.font = [UIFont systemFontOfSize:10];
    self.name.font = [UIFont systemFontOfSize:10];
    self.outcount.font = [UIFont systemFontOfSize:10];
    self.outdate.font = [UIFont systemFontOfSize:10];
    
    
    [self.contentView addSubview:self.title1];
    [self.contentView addSubview:self.price];
    [self.contentView addSubview:self.salecount];
    [self.contentView addSubview:self.place];
    [self.contentView addSubview:self.name];
    [self.contentView addSubview:self.outcount];
    [self.contentView addSubview:self.outdate];

}


-(void)setLayout
{
    

    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"title":self.title1,@"price":self.price,@"salecount":self.salecount,@"place":self.place,@"name":self.name,@"outcount":self.outcount,@"outdate":self.outdate};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[title]-|"] options:0 metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-20-[title(>=0)]-10-[price]-10-[salecount]-10-[outcount]"] options:NSLayoutFormatAlignAllLeft metrics:nil views:superviewdict]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[salecount]-40-[place]-40-[name]"] options:NSLayoutFormatAlignAllBottom metrics:nil views:superviewdict]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[outcount]-40-[outdate]"] options:NSLayoutFormatAlignAllBottom metrics:nil views:superviewdict]];
    
    
}

@end
