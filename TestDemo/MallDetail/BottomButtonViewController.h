//
//  BottomButtonViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomButtonViewController : UIViewController
@property(nonatomic,strong) UIButton *button1;
@property(nonatomic,strong) UIButton *button2;
@property(nonatomic,strong) UIButton *button3;
@end
