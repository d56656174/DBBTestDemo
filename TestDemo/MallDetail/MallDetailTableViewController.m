//
//  MallDetailTableViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/22.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetailTableViewController.h"
#import "MallDetaillRollingBannerTableViewCell.h"
#import "BussinessPromisTableViewCell.h"
#import "MallDetail1TableViewCell.h"
#import "MallDetail2TableViewCell.h"
#import "MHDatePicker.h"
#import "MMPickerView.h"
#import "MallDetail3TableViewCell.h"
#import "BottomButtonViewController.h"
#import "SetOutCountViewController.h"

static NSString *CellIdentifierDetailBanner = @"DetailBanner";
static NSString *CellIdentifierDetailPromiseBanner = @"DetailPromise";
static NSString *CellIdentifierDetailInfoBanner = @"DetailInfo";
static NSString *CellIdentifierDetailCountInfoBanner = @"DetailInfoCount";
static NSString *CellIdentifierDetailDateInfoBanner = @"DetailInfoDate";

@interface MallDetailTableViewController ()<UITextFieldDelegate,PassOutCountDetegate>
@property (strong, nonatomic) MHDatePicker *selectDatePicker;
//@property (nonatomic, strong) NSMutableArray *stringsArray;
//@property (nonatomic, strong) NSString * selectedString;
@property (strong,nonatomic) NSMutableArray *valueArray1;
@property (strong,nonatomic) UIView *bgview;
@property (strong,nonatomic) SetOutCountViewController *socv;

@end

@implementation MallDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.valueArray1 = [[NSMutableArray alloc] initWithObjects:@"0", @"1", @"2",@"出货日期:",@"订购数量:", nil];
    
    //self.tableView. = plain;
    //self.tableView =[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //BottomButtonViewController *bbv = [[BottomButtonViewController alloc] init];
    //bbv.view.backgroundColor = [UIColor greenColor];
    //[bbv.view setFrame:CGRectMake(0, 0, 100, 100)];
    //[self.view addSubview:bbv.view];
    //[self.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-88)];
    //[self.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100)];
    //[bbv.view setFrame:CGRectMake(0,self.view.frame.size.height-44,self.view.frame.size.width,22)];
    //[[[UIApplication sharedApplication] keyWindow] addSubview:bbv.view];
    //[self.tableView addSubview:bbv.view];
    //[self.view.superview addSubview:bbv.view];
    //self.tableView.tableFooterView = bbv.view;
    
    self.socv = [[SetOutCountViewController alloc] init];
    [self.socv.view setFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width,self.view.frame.size.height/2)];
    //socv.detegate = self;
    self.socv.textField.delegate=self;
    //[self.socv.alphaiView setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    self.bgview = [[UIView alloc] init];
    self.bgview.backgroundColor = [UIColor blackColor];
    self.bgview.alpha = 0.2;
    [self.bgview setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeSetcountView)];
    [self.bgview addGestureRecognizer:tap];
    //[self.view addSubview:self.alphaiView];
    //self.bgview =[]
    
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0)
    {
        MallDetaillRollingBannerTableViewCell *cell = (MallDetaillRollingBannerTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierDetailBanner];
        
        if(cell==nil)
        {
            cell = [[MallDetaillRollingBannerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDetailBanner];
            NSMutableArray *darr = [[NSMutableArray alloc] initWithArray:@[@"http://easyread.ph.126.net/G8GtEi-zmPQzvS5w7ScxmQ==/7806606224489671909.jpg"
                                                                           , @"https://c2.staticflickr.com/4/3345/5832660048_55f8b0935b.jpg"
                                                                           , @"http://epaper.syd.com.cn/sywb/res/1/20080108/42241199752656275.jpg",@"http://ww2.sinaimg.cn/mw600/0068quT8jw1evy4nuk799j30zk0nmq89.jpg",@"http://ww3.sinaimg.cn/mw600/cd7861dcgw1f3my8idgjwj20dw08pt94.jpg",@"http://ww2.sinaimg.cn/mw600/006fVPCvjw1f43gg54kmnj31kw11xtj1.jpg",@"http://ww3.sinaimg.cn/mw600/68f6e545gw1f43f46zc6xj20go0bp74p.jpg",@"http://ww3.sinaimg.cn/mw600/005WRDhNjw1f4391wuf2qj30ia0c6gmt.jpg",@"http://ww2.sinaimg.cn/mw600/005WRDhNjw1f4391rj5s1j30dw0e3acm.jpg",@"http://ww3.sinaimg.cn/mw600/005WRDhNjw1f4391w28glj30jg0czdiz.jpg"
                                                                           ]];
            [cell setDataArray1:darr];
            [cell initSubviews];

        }
        
        return cell;
    }
    else if(indexPath.section==1)
    {
        MallDetail1TableViewCell *cell = (MallDetail1TableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierDetailInfoBanner];
        
        if(cell==nil)
        {
            cell = [[MallDetail1TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDetailInfoBanner];
            //[cell initSubviews];
            cell.title1.text = @"大肥鹅大肥鹅大肥鹅大肥鹅大肥鹅大肥鹅";
            cell.price.text = @"价格";
            cell.salecount.text = @"当月销量";
            cell.place.text = @"北京";
            cell.name.text = @"大肥鹅";
            cell.outcount.text = @"出货数量";
            cell.outdate.text = @"出货日期";


        }
        
        return cell;
    }
    else if(indexPath.section==2)
    {
        BussinessPromisTableViewCell *cell = (BussinessPromisTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierDetailPromiseBanner];
        
        if(cell==nil)
        {
            cell = [[BussinessPromisTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDetailBanner];
            
        }
        
        return cell;
    }
    else if(indexPath.section==3)
    {
        MallDetail2TableViewCell *cell = (MallDetail2TableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierDetailCountInfoBanner];
        
        if(cell==nil)
        {
            cell = [[MallDetail2TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDetailCountInfoBanner];

            //cell.uilabel.font = [UIFont systemFontOfSize:10];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.uilabel.text = @"出货日期:";

        }
        NSString *valstring = [self.valueArray1 objectAtIndex:indexPath.section];
        cell.uivalue.text = valstring;
        return cell;
    }
    else if(indexPath.section==4)
    {
        MallDetail3TableViewCell *cell = (MallDetail3TableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierDetailDateInfoBanner];
        
        if(cell==nil)
        {
            cell = [[MallDetail3TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierDetailDateInfoBanner];
            
            //cell.uilabel.font = [UIFont systemFontOfSize:10];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            //cell.counttxt.delegate = self;
            //self.counttxt = cell.counttxt;
            //cell.counttxt.tag = indexPath.section;
            self.socv.textField.tag=indexPath.section;
        }
        cell.countlabel.text = self.valueArray1[4];
//        NSString *valstring = [self.valueArray1 objectAtIndex:indexPath.section];
//        cell.counttxt.text = valstring;
        return cell;
    }
    else
    {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0)
    {
        return 5;
    }
    else
    {
        return 5;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==4)
    {
        return 5;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        return 200;
    }
    else if(indexPath.section==1)
    {
        return 120;
    }
    else if(indexPath.section==2)
    {
        return 180;
    }
    else{
        return 60;
    }
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section==3)
    {
        _selectDatePicker = [[MHDatePicker alloc] init];
        _selectDatePicker.isBeforeTime = YES;
        _selectDatePicker.datePickerMode = UIDatePickerModeDate;
        
        __weak typeof(self) weakSelf = self;
        [_selectDatePicker didFinishSelectedDate:^(NSDate *selectedDate) {
            __strong typeof(self) strongSelf = weakSelf;
            //NSString *string = [NSString stringWithFormat:@"%@",[NSDate dateWithTimeInterval:3600*8 sinceDate:selectedDate]];
            //weakSelf.myLabel2.text = string;
            strongSelf.valueArray1[indexPath.section] = [strongSelf dateStringWithDate:selectedDate DateFormat:@"yyyy年MM月dd日"];
            [strongSelf.tableView reloadData];
        }];
    }
    else if(indexPath.section==4)
    {

        //[self.view.superview.inputViewController.navigationController pushViewController:socv animated:NO];
        //[self.navigationController pushViewController:socv animated:NO];
        
        [self showSetcountView];
        
        //[self presentViewController:socv animated:YES completion:nil];
        //[[[UIApplication sharedApplication] keyWindow] addSubview:socv.view];
//        [UIView animateWithDuration: 0.35 animations: ^{
//            self.bgview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.8,0.8);
//            self.bgview.center = CGPointMake(self.view.center.x, self.view.center.y-50);
//            self.socv.view.center = self.view.center;
//            self.socv.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        } completion: nil];
    }
    
}

- (NSString *)dateStringWithDate:(NSDate *)date DateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    NSString *str = [dateFormatter stringFromDate:date];
    return str ? str : @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];    //主要是[receiver resignFirstResponder]在哪调用就能把receiver对应的键盘往下收
    
    self.valueArray1[textField.tag]=textField.text;
    NSLog(@"%@",self.valueArray1[textField.tag]);
    [self.tableView reloadData];
    [self closeSetcountView];
    return YES;
}

-(void)showSetcountView
{
    [self.view addSubview:self.bgview];
    [self.view addSubview:self.socv.view];
}

-(void)closeSetcountView
{
    [self.socv.view removeFromSuperview];
    [self.bgview removeFromSuperview];
}

-(void)setOutCount:(NSString*)value
{
    self.valueArray1[4]=value;
    [self.tableView reloadData];
}

//开始编辑输入框的时候，软键盘出现，执行此事件
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //CGRect frame = textField.frame;
    int offset = self.view.frame.size.height/2+80 - (self.view.frame.size.height - 216.0);//键盘高度216
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        self.socv.view.frame = CGRectMake(0.0f, self.view.frame.size.height/2-offset, self.socv.view.frame.size.width, self.view.frame.size.height/2);
    
    [UIView commitAnimations];
}

//输入框编辑完成以后，将视图恢复到原始状态
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
