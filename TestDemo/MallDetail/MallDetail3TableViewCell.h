//
//  MallDetail3TableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallDetail3TableViewCell : UITableViewCell
@property(nonatomic,strong) UITextField *counttxt;
@property(nonatomic,strong) UILabel *countlabel;
-(void)initSubviews;
-(void)setLayout;
@end
