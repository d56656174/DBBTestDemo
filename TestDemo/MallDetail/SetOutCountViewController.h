//
//  SetOutCountViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/31.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PassOutCountDetegate <NSObject>
@required
-(void)setOutCount:(NSString*)value;
@end

@interface SetOutCountViewController : UIViewController
@property(strong,nonatomic) UITextField *textField;
//@property(nonatomic, retain)UIView *alphaiView;
@property (nonatomic, assign) id <PassOutCountDetegate> detegate;
@end

