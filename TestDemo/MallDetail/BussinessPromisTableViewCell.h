//
//  BussinessPromisTableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/23.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BussinessPromisTableViewCell : UITableViewCell
@property (strong,nonatomic)UILabel* nameLabel1;
@property (strong,nonatomic)UILabel* nameLabel2;
@property (strong,nonatomic)UILabel* nameLabel3;
@property (strong,nonatomic)UILabel* nameLabel4;
@property (strong,nonatomic)UILabel* nameLabel5;
-(void)initSubviews;
-(void)setLayout;
@end
