//
//  MallDetail3TableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetail3TableViewCell.h"

@implementation MallDetail3TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
        [self initSubviews];
        [self setLayout];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initSubviews
{
    //[self setImage:[[UIImageView alloc] init]];
//    [self setCounttxt:[[UITextField alloc] init]];
//    
//    
//    self.counttxt.font = [UIFont systemFontOfSize:12];
//    self.counttxt.placeholder = @"订购数量";
//    self.counttxt.keyboardType = UIKeyboardTypeDefault;
    self.countlabel = [[UILabel alloc] init];
    self.countlabel.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.countlabel];

    
}


-(void)setLayout
{
    
    
    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"countlabel":self.countlabel};
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[countlabel]"] options:0 metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-20-[countlabel]"] options:0 metrics:nil views:superviewdict]];
    
}


@end
