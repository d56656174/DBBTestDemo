//
//  BottomButtonViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/29.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "BottomButtonViewController.h"

@interface BottomButtonViewController ()

@end

@implementation BottomButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initSubviews];
    [self setLayout];
}



-(void)initSubviews
{
    //[self setImage:[[UIImageView alloc] init]];
    [self setButton1:[[UIButton alloc] init]];
    [self setButton2:[[UIButton alloc] init]];
    [self setButton3:[[UIButton alloc] init]];
    [self.view addSubview:self.button1];
    [self.view addSubview:self.button2];
    [self.view addSubview:self.button3];
    
    [self.button1 setTitle: @"购物车" forState: UIControlStateNormal];
    self.button1.titleLabel.font = [UIFont systemFontOfSize:14];
    self.button1.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.button1.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    self.button1.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:193.0f/255.0f blue:37.0f/255.0f alpha:0.5];
    
    
    
    [self.button2 setTitle: @"加入购物车" forState: UIControlStateNormal];
    self.button2.titleLabel.font = [UIFont systemFontOfSize:14];
    self.button2.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.button2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    self.button2.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:193.0f/255.0f blue:37.0f/255.0f alpha:0.5];
    
    [self.button3 setTitle: @"立即购买" forState: UIControlStateNormal];
    self.button3.titleLabel.font = [UIFont systemFontOfSize:14];
    self.button3.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.button3.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    self.button3.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:193.0f/255.0f blue:37.0f/255.0f alpha:0.5];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //[UIColor colorWithRed:255.0f green:193.0f blue:37.0f alpha:0.5];

}


-(void)setLayout
{
    
    
    for (UIView *view in self.view.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"button1":self.button1,@"button2":self.button2,@"button3":self.button3};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[button1]-1-[button2(button1)]-1-[button3(button1)]|"] options:NSLayoutFormatAlignAllBottom|NSLayoutFormatAlignAllTop metrics:nil views:superviewdict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[button1]|"] options:0 metrics:nil views:superviewdict]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
