//
//  MallDetailViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/31.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetailViewController.h"


@interface MallDetailViewController ()

@end

@implementation MallDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.detailtable = [[MallDetailTableViewController alloc] init];
    [self.view addSubview:self.detailtable.tableView];
    //[self.detailtable.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    self.buttonview = [[BottomButtonViewController alloc] init];
    //[self.buttonview.view setFrame:CGRectMake(0,self.view.frame.size.height-100,self.view.frame.size.width,44)];
    [self.view addSubview:self.buttonview.view];
    [self.view bringSubviewToFront:self.buttonview.view];
    //self.buttonview.view.backgroundColor = [UIColor redColor];
    
    [self setLayout];

}

-(void)setLayout
{
    
    
    for (UIView *view in self.view.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"topview":self.detailtable.tableView,@"bottomview":self.buttonview.view};
    

    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|[topview]|"] options:0 metrics:nil views:superviewdict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:    [NSString stringWithFormat:@"V:|[topview]-1-[bottomview(40)]|"] options:NSLayoutFormatAlignAllLeft|NSLayoutFormatAlignAllRight metrics:nil views:superviewdict]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
