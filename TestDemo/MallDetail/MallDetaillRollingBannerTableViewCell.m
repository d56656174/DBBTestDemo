//
//  MallDetaillRollingBannerTableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/22.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallDetaillRollingBannerTableViewCell.h"
#import <DYMRollingBanner/DYMRollingBannerVC.h>
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MallDetaillRollingBannerTableViewCell ()
@property(nonatomic,strong) DYMRollingBannerVC *_rollingBannerVC;
@end

@implementation MallDetaillRollingBannerTableViewCell




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
    }
    return self;
}

-(void)initSubviews
{
    [self rotatePic];
}

-(void)rotatePic
{
    
    
    self._rollingBannerVC = [DYMRollingBannerVC new];
    
    //[self.contentView :_rollingBannerVC];
    [self.contentView addSubview:self._rollingBannerVC.view];
    
    // The code below lays out the _rollingBannerVC's view using Masonry
    [self._rollingBannerVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.and.right.equalTo(self.contentView);
        //make.leading.top.equalTo(self.view).with.offset(-10);
        make.height.equalTo(@200);
    }];
    
    [self._rollingBannerVC didMoveToParentViewController:self.contentView];
    
    // 1. Set the inteval for rolling (optional, the default value is 1 sec)
    self._rollingBannerVC.rollingInterval = 3;
    
    // 2. set the placeholder image (optional, the default place holder is nil)
    self._rollingBannerVC.placeHolderImage = [UIImage imageNamed:@"default"];
    
    // 3. define the way how you load the image from a remote url
    [self._rollingBannerVC setRemoteImageLoadingBlock:^(UIImageView *imageView, NSString *imageUrlStr, UIImage *placeHolderImage) {
        [imageView sd_cancelCurrentImageLoad];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:placeHolderImage options:SDWebImageProgressiveDownload];
    }];
    
    // 4. setup the rolling images
    self._rollingBannerVC.rollingImages = [self dataArray1];
    
    // 5. add a handler when a tap event occours (optional, default do noting)
    [self._rollingBannerVC addBannerTapHandler:^(NSInteger whichIndex) {
        NSLog(@"banner tapped, index = %@", @(whichIndex));
    }];
    
    // 6. start auto rolling (optional, default does not auto roll)
    [self._rollingBannerVC startRolling];
}


-(void)setLayout
{
    //    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    //
    //    NSDictionary *superviewdict = @{@"_pic":self._rollingBannerVC,@"supperview":self.contentView};
    //    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[_pic(%f)]-<=1-[supperview]",80.0] options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
    //    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[_pic(%f)]-<=1-[supperview]",80.0] options:NSLayoutFormatAlignAllCenterX metrics:nil views:superviewdict]];
}

@end
