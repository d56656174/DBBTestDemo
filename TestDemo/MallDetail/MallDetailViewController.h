//
//  MallDetailViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/31.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallDetailTableViewController.h"
#import "BottomButtonViewController.h"
@interface MallDetailViewController : UIViewController
@property(strong,nonatomic) MallDetailTableViewController *detailtable;
@property(strong,nonatomic) BottomButtonViewController *buttonview;
@end
