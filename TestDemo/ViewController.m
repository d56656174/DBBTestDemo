//
//  ViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 15/11/20.
//  Copyright © 2015年 dongbingbin. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "TESTPerson.h"
#import "TableBarViewController.h"
#import "LoginViewController.h"
#import "BasicInfoTableViewController.h"
#import "DTKDropdownMenuView.h"
#import <DYMRollingBanner/DYMRollingBannerVC.h>
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MallIndexTableViewController.h"
#import "MallListTableViewController.h"
#import "MaillListViewController.h"
#import "MallDetailTableViewController.h"
#import "MallDetailViewController.h"

#define kWinH [[UIScreen mainScreen] bounds].size.height
#define kWinW [[UIScreen mainScreen] bounds].size.width
@interface ViewController ()<UISearchBarDelegate, UIScrollViewDelegate>
@property (strong, nonatomic) UIButton *bgButton;
@property (strong, nonatomic) UIView *bgView;
@property (nonatomic, strong) UISearchBar *searchBar;
@end

typedef NSString * (^ViewControllerHandler) (int,int);

@implementation ViewController


__weak NSString *string_weak_ = nil;
- (void)testAutolayout {
    UILabel *Label1 = [[UILabel alloc] init];
    Label1.translatesAutoresizingMaskIntoConstraints = NO;
    Label1.text = @"Label1:";
    Label1.backgroundColor = [UIColor greenColor];
    [self.view addSubview:Label1];
    
    
    
    UILabel *Label2 = [[UILabel alloc] init];
    Label2.translatesAutoresizingMaskIntoConstraints = NO;
    Label2.text = @"Label2:";
    Label2.backgroundColor = [UIColor greenColor];
    [self.view addSubview:Label2];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(Label1, Label2);
    
    NSString *hVFL111 = @"H:|-[Label1(100)]-20-[Label2(150)]";
    NSArray *hCons111 = [NSLayoutConstraint constraintsWithVisualFormat:hVFL111 options:NSLayoutFormatAlignAllTop|NSLayoutFormatAlignAllBottom metrics:nil views:views];
    [self.view addConstraints:hCons111];
    
    NSString *hVFL112 = @"V:|-100-[Label1(100)]";
    NSArray *hCons112 = [NSLayoutConstraint constraintsWithVisualFormat:hVFL112 options:0 metrics:nil views:views];
    [self.view addConstraints:hCons112];
    
    
    //居中设置
    UILabel *userNameLabel = [[UILabel alloc] init];
    userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    userNameLabel.text = @"用户名:";
    userNameLabel.backgroundColor = [UIColor greenColor];
    //[self.view addSubview:userNameLabel];
    
    UITextField * txt = [[UITextField alloc] init];
    txt.translatesAutoresizingMaskIntoConstraints = NO;
    txt.placeholder = @"请输入用户名";
    //[self.view addSubview:txt];
    
    UIView *uiview = [[UIView alloc] init];
    [uiview addSubview:userNameLabel];
    [uiview addSubview:txt];
    uiview.backgroundColor = [UIColor blueColor];
    //uiview.frame = CGRectMake(50, 50, 20, 300);
    uiview.translatesAutoresizingMaskIntoConstraints=NO;
    NSString *hVFL11 = @"H:|-[userNameLabel(100)]-20-[txt(150)]";
    NSArray *hCons11 = [NSLayoutConstraint constraintsWithVisualFormat:hVFL11 options:0 metrics:nil views:@{@"userNameLabel":userNameLabel,@"txt":txt,@"superView":uiview}];
    [uiview addConstraints:hCons11];
    
    NSString *vVFL12 = @"V:|-[userNameLabel(20)]";
    NSArray *vCons12 = [NSLayoutConstraint constraintsWithVisualFormat:vVFL12 options:0 metrics:nil views:@{@"userNameLabel":userNameLabel,@"txt":txt,@"superView":uiview}];
    [uiview addConstraints:vCons12];
    
    NSString *vVFL13 = @"V:|-[txt(20)]";
    NSArray *vCons13 = [NSLayoutConstraint constraintsWithVisualFormat:vVFL13 options:0 metrics:nil views:@{@"userNameLabel":userNameLabel,@"txt":txt,@"superView":uiview}];
    [uiview addConstraints:vCons13];
    
    
    [self.view addSubview:uiview];
    
    NSString *hVFL = @"H:[uiview(280)]-(<=1)-[superView]";
    NSArray *hCons = [NSLayoutConstraint constraintsWithVisualFormat:hVFL options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{@"uiview":uiview,@"superView":self.view}];
    [self.view addConstraints:hCons];
    
    NSString *vVFL1 = @"V:[uiview(40)]-(<=1)-[superView]";
    NSArray *vCons1 = [NSLayoutConstraint constraintsWithVisualFormat:vVFL1 options:NSLayoutFormatAlignAllCenterX metrics:nil views:@{@"uiview":uiview,@"superView":self.view}];
    [self.view addConstraints:vCons1];
    ///居中设置
}
-(void)action:(id)sender{
    //这个sender其实就是UIButton，因此通过sender.tag就可以拿到刚才的参数
    //int i = [sender tag];
    //login view
    //LoginViewController *login = [[LoginViewController alloc] init];
    //[self presentViewController:login animated:YES completion:nil];
    
    //basic info
    //BasicInfoTableViewController *login = [[BasicInfoTableViewController alloc] init];
    //[self.navigationController pushViewController:login animated:YES];
    //[self presentViewController:login animated:YES completion:nil];
    
    //mallindex
    //MallIndexTableViewController *login = [[MallIndexTableViewController alloc] init];
    //[self.navigationController pushViewController:login animated:YES];
    
    //malllist MallListTableViewController
//    MallListTableViewController *login = [[MallListTableViewController alloc] init];
//    [self.navigationController pushViewController:login animated:YES];
    
    //MaillListViewController
    //MaillListViewController *login = [[MaillListViewController alloc] init];
    //[self.navigationController pushViewController:login animated:YES];
    
    
    //MaillDetailViewController
    MallDetailViewController *login = [[MallDetailViewController alloc] init];
    [self.navigationController pushViewController:login animated:YES];

    
}

-(void)action2:(id)sender{
    _searchBar = [UISearchBar new];
    _searchBar.delegate = self;
    _searchBar.placeholder = @"请输入关键字";
//    ((AppDelegate *)[UIApplication sharedApplication].delegate).inNightMode = [Config getMode];
//    if (((AppDelegate *)[UIApplication sharedApplication].delegate).inNightMode) {
//        _searchBar.keyboardAppearance = UIKeyboardAppearanceDark;
//        _searchBar.barTintColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
//    }
    
    self.navigationItem.titleView = _searchBar;

}

-(void)action1:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
    //半透明背景按钮
    _bgButton = [[UIButton alloc] init];
    [self.view addSubview:_bgButton];
    [_bgButton addTarget:self action:@selector(dismissSearchView) forControlEvents:UIControlEventTouchUpInside];
    _bgButton.backgroundColor = [UIColor blackColor];
    _bgButton.alpha = 0.0;
    _bgButton.frame = CGRectMake(0, 0, kWinW, kWinH);
    
    
    
    //时间选择View
//    _pickerView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MHSelectPickerView class]) owner:self options:nil].lastObject;
//    [self addSubview:_pickerView];
//    _pickerView.frame = CGRectMake(0, kWinH, kWinW, kPVH);
//    [_pickerView.cancleBtn addTarget:self action:@selector(dismissDatePicker) forControlEvents:UIControlEventTouchUpInside];
//    [_pickerView.confirmBtn addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [_pickerView.datePicker addTarget:self action:@selector(datePickerValueChange:) forControlEvents:UIControlEventValueChanged];
    
    //DatePicker属性设置
    // _selectDate = [[NSDate date] dateByAddingTimeInterval:60*60];
//    _selectDate = [NSDate new];
//    _pickerView.datePicker.date = _selectDate;
//    _pickerView.datePicker.minimumDate = _selectDate;
//    _pickerView.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
//    
    [self pushDatePicker];
}
-(void)dismissSearchView
{
    //[self.navigationItem.titleView removeFromSuperview];
}

//出现
- (void)pushDatePicker
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        //weakSelf.pickerView.frame = CGRectMake(0, kWinH - kPVH, kWinW, kPVH);
        weakSelf.bgButton.alpha = 0.2;
    }];
}
- (void)addTitleMenu
{
    DTKDropdownItem *item0 = [DTKDropdownItem itemWithTitle:@"dropdownItem0" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    DTKDropdownItem *item1 = [DTKDropdownItem itemWithTitle:@"dropdownItem1" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    DTKDropdownItem *item2 = [DTKDropdownItem itemWithTitle:@"dropdownItem2" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    DTKDropdownItem *item3 = [DTKDropdownItem itemWithTitle:@"Item3" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    DTKDropdownMenuView *menuView = [DTKDropdownMenuView dropdownMenuViewForNavbarTitleViewWithFrame:CGRectMake(0, 0, 200.f, 44.f) dropdownItems:@[item0,item1,item2,item3]];
    menuView.currentNav = self.navigationController;
    menuView.dropWidth = 150.f;
    menuView.titleFont = [UIFont systemFontOfSize:18.f];
   // menuView.textColor = ColorWithRGB(102.f, 102.f, 102.f);
    menuView.textFont = [UIFont systemFontOfSize:13.f];
   // menuView.cellSeparatorColor = ColorWithRGB(229.f, 229.f, 229.f);
    menuView.textFont = [UIFont systemFontOfSize:14.f];
    menuView.animationDuration = 0.2f;
    menuView.selectedIndex = 3;
    menuView.cellSeparatorColor = [UIColor greenColor];
    self.navigationItem.titleView = menuView;
    
}

-(void)rotatePic
{
    DYMRollingBannerVC *_rollingBannerVC;
    
    _rollingBannerVC = [DYMRollingBannerVC new];
    
    [self addChildViewController:_rollingBannerVC];
    [self.view addSubview:_rollingBannerVC.view];
    
    // The code below lays out the _rollingBannerVC's view using Masonry
    [_rollingBannerVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.and.right.equalTo(self.view);
        //make.leading.top.equalTo(self.view).with.offset(-10);
        make.height.equalTo(@200);
    }];
    
    [_rollingBannerVC didMoveToParentViewController:self];
    
    // 1. Set the inteval for rolling (optional, the default value is 1 sec)
    _rollingBannerVC.rollingInterval = 5;
    
    // 2. set the placeholder image (optional, the default place holder is nil)
    _rollingBannerVC.placeHolderImage = [UIImage imageNamed:@"default"];
    
    // 3. define the way how you load the image from a remote url
    [_rollingBannerVC setRemoteImageLoadingBlock:^(UIImageView *imageView, NSString *imageUrlStr, UIImage *placeHolderImage) {
        [imageView sd_cancelCurrentImageLoad];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:placeHolderImage options:SDWebImageProgressiveDownload];
    }];
    
    // 4. setup the rolling images
    _rollingBannerVC.rollingImages = @[@"http://easyread.ph.126.net/G8GtEi-zmPQzvS5w7ScxmQ==/7806606224489671909.jpg"
                                       , @"https://c2.staticflickr.com/4/3345/5832660048_55f8b0935b.jpg"
                                       , @"http://epaper.syd.com.cn/sywb/res/1/20080108/42241199752656275.jpg"
                                       ];
    
    // 5. add a handler when a tap event occours (optional, default do noting)
    [_rollingBannerVC addBannerTapHandler:^(NSInteger whichIndex) {
        NSLog(@"banner tapped, index = %@", @(whichIndex));
    }];
    
    // 6. start auto rolling (optional, default does not auto roll)
    [_rollingBannerVC startRolling];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(50, 50, 50, 50)];
    [button setTitle:@"add" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor greenColor];
    [button addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button];
    
    UIButton* button1 = [[UIButton alloc] initWithFrame:CGRectMake(150, 150, 50, 50)];
    [button1 setTitle:@"add" forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor greenColor];
    [button1 addTarget:self action:@selector(action1:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button1];
    
    
    UIButton* button2 = [[UIButton alloc] initWithFrame:CGRectMake(150, 250, 50, 50)];
    [button2 setTitle:@"add2" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor greenColor];
    [button2 addTarget:self action:@selector(action2:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button2];
    
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"123456 654321"];
    [str addAttribute:NSForegroundColorAttributeName
                value:[UIColor redColor]
                range:NSMakeRange(0,5)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(100,100,100,100)];
    label.attributedText = str;
    [self.view addSubview:label];
    [self addTitleMenu];
    //[self rotatePic];
//    [self initSubviews];
//    [self setLayout];
    
    //[self testAutolayout];
    
    
    
    
    //1.添加两个控件
//    UIView *blueView = [[UIView alloc] init];
//    blueView.backgroundColor = [UIColor greenColor];
//    
//    blueView.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.view addSubview:blueView];
//    
//    UIView *redView = [[UIView alloc] init];
//    redView.backgroundColor = [UIColor redColor];
//    redView.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.view addSubview:redView];
//    
//    
//    UILabel *userNameLabel = [[UILabel alloc] init];
//    userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
//    userNameLabel.text = @"用户名:";
//    [self.view addSubview:userNameLabel];
//    
//    UITextField * txt = [[UITextField alloc] init];
//    txt.translatesAutoresizingMaskIntoConstraints = NO;
//    txt.placeholder = @"请输入用户名";
//    [self.view addSubview:txt];
//    
//    UILabel *pwdLabel = [[UILabel alloc] init];
//    pwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
//    pwdLabel.text = @"密    码:";
//    [self.view addSubview:pwdLabel];
//    
//    UITextField * pwdtxt = [[UITextField alloc] init];
//    pwdtxt.translatesAutoresizingMaskIntoConstraints = NO;
//    pwdtxt.placeholder = @"请输入密码";
//    [self.view addSubview:pwdtxt];
//    
//    //2.添加约束
//    //2.1水平方向的约束
//    NSString *hVFL = @"H:[userNameLabel(60)]-20-[txt]";
//    NSArray *hCons = [NSLayoutConstraint constraintsWithVisualFormat:hVFL options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{@"userNameLabel":userNameLabel,@"txt":txt}];
//    [self.view addConstraints:hCons];
//    
//    NSString *hVFL1 = @"H:[pwdLabel(60)]-20-[pwdtxt]";
//    NSArray *hCons1 = [NSLayoutConstraint constraintsWithVisualFormat:hVFL1 options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{@"pwdLabel":pwdLabel,@"pwdtxt":pwdtxt}];
//    [self.view addConstraints:hCons1];
//    
//    //2.2垂直方向的约束
//    NSString *vVFL = @"V:|-100-[txt]-10-[pwdtxt]";
//    NSArray *vCons = [NSLayoutConstraint constraintsWithVisualFormat:vVFL options:NSLayoutFormatAlignAllCenterX metrics:nil views:@{@"txt":txt,@"pwdtxt":pwdtxt}];
//    [self.view addConstraints:vCons];
//    
//    //2.2垂直方向的约束
//    NSString *vVFL1 = @"V:|-100-[userNameLabel]-10-[pwdLabel]";
//    NSArray *vCons1 = [NSLayoutConstraint constraintsWithVisualFormat:vVFL1 options:NSLayoutFormatAlignAllCenterX metrics:nil views:@{@"userNameLabel":userNameLabel,@"pwdLabel":pwdLabel}];
//    [self.view addConstraints:vCons1];
    //UIStoryboard *discoverSB = [UIStoryboard storyboardWithName:@"Discover" bundle:nil];
    //UINavigationController *discoverNav = [discoverSB instantiateViewControllerWithIdentifier:@"Nav"];
    
    
   // UIStoryboard *homepageSB = [UIStoryboard storyboardWithName:@"Homepage" bundle:nil];
   // UINavigationController *homepageNav = [homepageSB instantiateViewControllerWithIdentifier:@"Nav"];
    
    //TableBarViewController *tarbarview = [[TableBarViewController alloc] init];
    //self.navigationController =homepageSB;
    //self.tabBar.translucent = NO;
    //[self.navigationController pushViewController:tarbarview animated:YES];
    //[self.view addSubview:newsSVC];
//    TESTPerson *tp = [[TESTPerson alloc] init];
//    [tp sayHello];
//    _scrollview = [[UIScrollView alloc] init];
//    
    NSString *temp = @"123";
    NSString *temp2 = @"123";
    
    NSLog(@"%p",temp);
    NSLog(@"%p",temp2);
    
    NSString *temp3 = temp2;
    temp2 = [temp2 stringByAppendingString:@"456"];//新建了一个NSString对象，所以地址变了
    
    NSLog(@"%p",temp);
    NSLog(@"%p",temp2);
    NSLog(@"%p",temp3);
    [self testNSStringAddress];
    [self testWeak];
    [self testStrong];
//
//    
//    [self testMutableNSString];
//    
//    UIButton *btn1 = [[UIButton alloc] init];
//    UIButton *btn2 = [[UIButton alloc] init];
//    
//    NSLog(@"btn1:%p",btn1);
//    NSLog(@"btn2:%p",btn2);
    //[self testDispatch_get_main_async];
    
    // Do any additional setup after loading the view, typically from a nib. test
}


-(void)initSubviews
{
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imageView.image = [UIImage imageNamed:@"loginbackground.png"];
    //imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    self.boxview = [[UIView alloc] init];
    self.boxview.translatesAutoresizingMaskIntoConstraints = NO;
    //self.boxview.backgroundColor = [UIColor blueColor];
    [self.view addSubview:self.boxview];
    
    
    self.phoneLabel = [[UIImageView alloc] init];
    //self.phoneLabel.backgroundColor = [UIColor greenColor];
    self.phoneLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    //UIImageView* iconimageView = [[UIImageView alloc] initWithFrame:self.phoneLabel.frame];
    //NSLog(@"self.phoneLabel.frame:%@",self.phoneLabel.frame);
    self.phoneLabel.image = [UIImage imageNamed:@"downicon.png"];
    //self.phoneLabel.contentMode = UIViewContentModeScaleAspectFit;
    //UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"downicon.png"]];
    //[myLabel setBackgroundColor:color];
    //[self.phoneLabel setBackgroundColor:color];
    
    [self.boxview addSubview:self.phoneLabel];
    
    self.pwdLabel = [[UILabel alloc] init];
    self.pwdLabel.backgroundColor = [UIColor whiteColor];
    self.pwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.pwdLabel.text=@"显示";
    self.pwdLabel.textAlignment = NSTextAlignmentCenter;
    self.pwdLabel.font = [UIFont fontWithName:@"Arial" size:10.0f];
    [self.boxview addSubview:self.pwdLabel];
    
    self.fogetpwdLabel = [[UILabel alloc] init];
    //self.fogetpwdLabel.backgroundColor = [UIColor greenColor];
    self.fogetpwdLabel.text = @"忘记密码?";
    self.fogetpwdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.boxview addSubview:self.fogetpwdLabel];
    
    self.phonetxt = [[UITextField alloc] init];
    self.phonetxt.translatesAutoresizingMaskIntoConstraints = NO;
    self.phonetxt.placeholder = @"手机号";
    [self.boxview addSubview:self.phonetxt];
    
    self.pwdtxt = [[UITextField alloc] init];
    self.pwdtxt.translatesAutoresizingMaskIntoConstraints = NO;
    self.pwdtxt.placeholder = @"6-16位字母和数字的密码";
    self.pwdtxt.secureTextEntry = YES;
    //self.pwdtxt.placeholder =@"密码";
    [self.boxview addSubview:self.pwdtxt];
    
//    self.pwdtxt = [[UITextField alloc] init];
//    self.pwdtxt.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.boxview addSubview:self.pwdtxt];

    
    self.lineView1 = [[UIView alloc] init];
    self.lineView1.translatesAutoresizingMaskIntoConstraints = NO;
    self.lineView1.backgroundColor = [UIColor grayColor];
    
    [self.boxview addSubview:self.lineView1];
    
    self.lineView2 = [[UIView alloc] init];
    self.lineView2.translatesAutoresizingMaskIntoConstraints = NO;
    self.lineView2.backgroundColor = [UIColor grayColor];
    [self.boxview addSubview:self.lineView2];
    
    
    
    
    
    
    self.signinBtn = [[UIButton alloc] init];
    [self.signinBtn setTitle:@"登录" forState:UIControlStateNormal];
    self.signinBtn.translatesAutoresizingMaskIntoConstraints = NO;
    self.signinBtn.backgroundColor = [UIColor whiteColor];
    [self.signinBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.signinBtn.titleLabel setTextColor:[UIColor blackColor]];
    [self.boxview addSubview:self.signinBtn];
    
    self.signupBtn = [[UIButton alloc] init];
    [self.signupBtn setTitle:@"注册" forState:UIControlStateNormal];
    self.signupBtn.translatesAutoresizingMaskIntoConstraints = NO;
    self.signupBtn.backgroundColor = [UIColor whiteColor];
    [self.signupBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.boxview addSubview:self.signupBtn];
    
    //UIColor *bgColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginbackground"]];
                        
    //[self.view setBackgroundColor:bgColor];

    
}

-(void)setLayout
{
    //set boxview layout
    NSDictionary *box1dict = NSDictionaryOfVariableBindings(_boxview, _phoneLabel, _phonetxt, _pwdLabel, _pwdtxt,_lineView1,_lineView2,_fogetpwdLabel,_signinBtn,_signupBtn);
    
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_signinBtn(100)]-120-[_signupBtn(100)]" options:NSLayoutFormatAlignAllTop|NSLayoutFormatAlignAllBottom metrics:nil views:box1dict]];

    
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_phonetxt]-8-[_phoneLabel(26)]-20-|" options:NSLayoutFormatAlignAllTop|NSLayoutFormatAlignAllBottom metrics:nil views:box1dict]];
    
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_pwdtxt]-8-[_pwdLabel(56)]-20-|" options:NSLayoutFormatAlignAllTop|NSLayoutFormatAlignAllBottom metrics:nil views:box1dict]];
    
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[_phonetxt(20)]-8-[_lineView1(1)]-8-[_pwdtxt(20)]-8-[_lineView2(1)]-8-[_fogetpwdLabel]-20-[_signinBtn(50)]" options:NSLayoutFormatAlignAllLeft metrics:nil views:box1dict]];
    
    
    
    
   

    
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_lineView2]-20-|" options:0 metrics:nil views:box1dict]];
    [self.boxview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_lineView1]-20-|" options:0 metrics:nil views:box1dict]];
    
    NSDictionary *superviewdict = @{@"_boxview":_boxview,@"supperview":self.view};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_boxview]-(<=1)-[supperview]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_boxview]-(<=1)-[supperview]" options:NSLayoutFormatAlignAllCenterX metrics:nil views:superviewdict]];
    //NSDictionary *superviewdict = @{@"_boxview":_boxview,@"supperview":self.view};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_boxview]-20-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
}


-(void)testWeak
{
    id __weak obj0=nil;
    {
        id obj1 = [[NSObject alloc] init];
        obj0=obj1;
        NSLog(@"obj0 A:%@",obj0);
    }
    NSLog(@"obj0 B:%@",obj0);
}

-(void)testStrong
{
    id obj0=nil;
    {
        id obj1 = [[NSObject alloc] init];
        obj0 = obj1;
        NSLog(@"obj0 A:%@",obj0);
    }
    NSLog(@"obj0 B:%@",obj0);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"string: %@", string_weak_);
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"string: %@", string_weak_);
}

//测试autorelease 来自 http://www.cocoachina.com/ios/20150610/12093.html
-(void)testAutorelease
{
    // 场景 1
    //    NSString *string = [NSString stringWithFormat:@"leichunfeng"];
    //    string_weak_ = string;
    
    // 场景 2
    //        @autoreleasepool {
    //            NSString *string = [NSString stringWithFormat:@"leichunfeng"];
    //            string_weak_ = string;
    //        }
    // 场景 3
    //        NSString *string = nil;
    //        @autoreleasepool {
    //            string = [NSString stringWithFormat:@"leichunfeng"];
    //            string_weak_ = string;
    //        }
    //    NSLog(@"string: %@", string_weak_);
}


//nsmutablestring 初始化对象，地址都是不一样的
-(void)testMutableNSString
{
    NSMutableString *temp1 = [[NSMutableString alloc] initWithFormat:@"123"];
    [temp1 appendString:@"123"];
    NSMutableString *temp2 = temp1;
    NSMutableString *temp3 = [[NSMutableString alloc] initWithFormat:@"123"];
    [temp3 appendString:@"123"];
    NSLog(@"temp1:%p",temp1);
    NSLog(@"temp2:%p",temp2);
    NSLog(@"temp3:%p",temp3);
    [temp2 appendString:@"456"];
    
    NSLog(@"temp1:%p",temp1);
    NSLog(@"temp2:%p",temp2);
    
    
    NSMutableString *temp4 = [[NSMutableString alloc] initWithFormat:@"123"];

    NSMutableString *temp5 = [[NSMutableString alloc] initWithFormat:@"123"];
    
    NSLog(@"temp4:%p",temp4);
    NSLog(@"temp5:%p",temp5);
}









-(void)testNSStringAddress
{
    NSString *str1 = @"hello";//地址：0x106ebaff0
    
    NSString *str2 = @"hello";//地址：0x106ebaff0
    
    NSString *str3 = [NSString stringWithFormat:@"hello"];//地址改变：0xa00006f6c6c65685
    
    NSString *str4 = [NSString stringWithString:@"hello"];//地址：0x106ebaff0
    
    NSString *str5 = [[NSString alloc]initWithString:@"hello"];//地址：0x106ebaff0
    
    NSLog(@"%p //%p //%p // %p //%p",str1,str2,str3,str4,str4,str5);
    
    //0x92a88 //0x92a88 //0x796b96b0 // 0x92a88 //0x92a88
}








-(void)testDispatch_sync
{
    dispatch_queue_t concurrentQueue = dispatch_queue_create("my.concurrent.queue", DISPATCH_QUEUE_CONCURRENT);
    NSLog(@"1");
    dispatch_sync(concurrentQueue, ^(){
        NSLog(@"2");
        [NSThread sleepForTimeInterval:10];
        NSLog(@"3");
    });
    NSLog(@"4");
}

-(void)testDispatch_async
{
    dispatch_queue_t concurrentQueue = dispatch_queue_create("my.concurrent.queue", DISPATCH_QUEUE_CONCURRENT);
    NSLog(@"1");
    dispatch_async(concurrentQueue, ^(){
        NSLog(@"2");
        [NSThread sleepForTimeInterval:10];
        NSLog(@"3");
    });
    NSLog(@"4");
}

-(void)testDispatch_global
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0); // 并行queue
    
    for ( int i = 0; i<10000; i++) {
        dispatch_async(aQueue, ^{
            NSLog(@"%d",i);
        });
    }
}


-(void)testBlock
{
    void(^testblock)()=^{
        NSLog(@"%@",@"testblock");
    };
    testblock();
    
    ViewControllerHandler handler = ^(int a,int b){
        NSLog(@"%d",a+b);
        return @"handler";
    };
    
    NSLog(@"%@",handler(1,5));
}

-(void)testDispatch_get_main_sync
{
    dispatch_queue_t manqueue = dispatch_get_main_queue();
    dispatch_sync(manqueue, ^{
        NSLog(@"%@",@"1");
    });
    NSLog(@"%@",@"2");
}

-(void)testDispatch_get_main_async
{
    dispatch_queue_t manqueue = dispatch_get_main_queue();
    dispatch_async(manqueue, ^{
        NSLog(@"%@",@"1");
    });
    NSLog(@"%@",@"2");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
