//
//  MaillListViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/15.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MaillListViewController.h"
#import "MallListTableViewController.h"
#import "Product.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DTKDropdownMenuView.h"
#import "Masonry.h"
#import "LrdSuperMenu.h"
#define kWinH [[UIScreen mainScreen] bounds].size.height
#define kWinW [[UIScreen mainScreen] bounds].size.width
@interface MaillListViewController ()<LrdSuperMenuDataSource, LrdSuperMenuDelegate,UISearchBarDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) LrdSuperMenu *menu;
@property (nonatomic, strong) NSArray *sort;
@property (nonatomic, strong) NSArray *choose;
@property (nonatomic, strong) NSArray *classify;
@property (nonatomic, strong) NSArray *jiachang;
@property (nonatomic, strong) NSArray *difang;
@property (nonatomic, strong) NSArray *tese;
@property (nonatomic, strong) NSArray *rihan;
@property (nonatomic, strong) NSArray *xishi;
@property (nonatomic, strong) NSArray *shaokao;
@property (strong, nonatomic) UIButton *bgNavigationButton;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (strong, nonatomic) DTKDropdownMenuView *menuView;
@property (nonatomic, strong) UIView *backGroundView;

@end

@implementation MaillListViewController

-(void)initFilter
{
    self.classify = @[@"全部", @"新店特惠", @"连锁餐厅", @"家常快餐", @"地方菜", @"特色小吃", @"日韩料理", @"西式快餐", @"烧烤海鲜"];
    self.sort = @[@"排序", @"智能排序", @"销量最高", @"距离最近", @"评分最高", @"起送价最低", @"送餐速度最快"];
    self.choose = @[@"筛选", @"立减优惠", @"预定优惠", @"特价优惠", @"折扣商品", @"进店领券", @"下单返券"];
    self.jiachang = @[@"家常炒菜", @"黄焖J8饭", @"麻辣烫", @"盖饭"];
    self.difang = @[@"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜"];
    self.tese = @[@"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜"];
    self.rihan = @[@"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜"];
    self.xishi = @[@"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜"];
    self.shaokao = @[@"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜", @"湘菜"];
    
    
    _menu = [[LrdSuperMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:44];
    _menu.delegate = self;
    _menu.dataSource = self;
    [self.view addSubview:_menu];
    
    [_menu selectDeafultIndexPath];
}

-(void)initDTK
{
    DTKDropdownItem *item3 = [DTKDropdownItem itemWithTitle:@"商品" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    DTKDropdownItem *item4 = [DTKDropdownItem itemWithTitle:@"商铺" callBack:^(NSUInteger index, id info) {
        NSLog(@"dropdownItem%lu",(unsigned long)index);
    }];
    if(_menuView==nil)
    {
        _menuView = [DTKDropdownMenuView dropdownMenuViewForNavbarTitleViewWithFrame:CGRectMake(0, 0, 50.f, 44.f) dropdownItems:@[item3,item4]];
        _menuView.currentNav = self.navigationController;
        _menuView.dropWidth = 150.f;
        _menuView.titleFont = [UIFont systemFontOfSize:18.f];
        _menuView.textColor = [UIColor blueColor];
        _menuView.textFont = [UIFont systemFontOfSize:13.f];
        // menuView.cellSeparatorColor = ColorWithRGB(229.f, 229.f, 229.f);
        _menuView.textFont = [UIFont systemFontOfSize:14.f];
        _menuView.animationDuration = 0.2f;
        _menuView.selectedIndex = 0;
        _menuView.cellSeparatorColor = [UIColor greenColor];
        //menuView.backgroundColor=[UIColor greenColor];
    }
    self.navigationItem.titleView = _menuView;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"搜索"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(searchKeyWord)];

    
    
}

-(void)searchKeyWord
{
    if(_backGroundView==nil)
    {
        _backGroundView = [[UIView alloc] init];
        _backGroundView.frame = CGRectMake(0, 0, kWinW, kWinH);
        _backGroundView.backgroundColor = [UIColor blackColor];
        _backGroundView.alpha = 0.1;
        
        UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSearchView:)];
        [_backGroundView addGestureRecognizer:backTap];
    }
    [self.view addSubview:_backGroundView];

    [self.navigationItem.titleView removeFromSuperview];
    if(_searchBar==nil)
    {
        _searchBar = [[UISearchBar alloc] init];
    }
    _searchBar.delegate = self;
    _searchBar.placeholder = @"请输入关键字";
    self.navigationItem.titleView = _searchBar;//把导航栏的titleview替换成searchbar
    [self.navigationItem setHidesBackButton:YES];//返回键隐藏
    [self.navigationItem setRightBarButtonItem:nil];//右键设置为空
}




-(void)removeSearchView:(UITapGestureRecognizer *)gesture
{

    [self removeSearchViewFromSuper];
}

-(void)removeSearchViewFromSuper
{
    [_backGroundView removeFromSuperview];//背景去掉
    [self.navigationItem.titleView removeFromSuperview];
    _menuView = nil;
    [self initDTK];
    
    [self.navigationItem setHidesBackButton:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.mallListTable  = [[MallListTableViewController alloc] init];
    [self.view addSubview:self.mallListTable.tableView];
    [self.mallListTable.tableView setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self initFilter];
    [self initDTK];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"导航栏  640×88"] forBarMetrics:UIBarMetricsDefault];
    //[self setLayout];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - filter menuLrdSuperMenu.h delegate

- (NSInteger)numberOfColumnsInMenu:(LrdSuperMenu *)menu {
    return 3;
}

- (NSInteger)menu:(LrdSuperMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    if (column == 0) {
        return self.classify.count;
    }else if(column == 1) {
        return self.sort.count;
    }else {
        return self.choose.count;
    }
}

- (NSString *)menu:(LrdSuperMenu *)menu titleForRowAtIndexPath:(LrdIndexPath *)indexPath {
    if (indexPath.column == 0) {
        return self.classify[indexPath.row];
    }else if(indexPath.column == 1) {
        return self.sort[indexPath.row];
    }else {
        return self.choose[indexPath.row];
    }
}

- (NSString *)menu:(LrdSuperMenu *)menu imageNameForRowAtIndexPath:(LrdIndexPath *)indexPath {
    if (indexPath.column == 0 || indexPath.column == 1) {
        return @"baidu";
    }
    return nil;
}

- (NSString *)menu:(LrdSuperMenu *)menu imageForItemsInRowAtIndexPath:(LrdIndexPath *)indexPath {
    if (indexPath.column == 0 && indexPath.item >= 0) {
        return @"baidu";
    }
    return nil;
}

- (NSString *)menu:(LrdSuperMenu *)menu detailTextForRowAtIndexPath:(LrdIndexPath *)indexPath {
    if (indexPath.column < 2) {
        return [@(arc4random()%1000) stringValue];
    }
    return nil;
}

- (NSString *)menu:(LrdSuperMenu *)menu detailTextForItemsInRowAtIndexPath:(LrdIndexPath *)indexPath {
    return [@(arc4random()%1000) stringValue];
}

- (NSInteger)menu:(LrdSuperMenu *)menu numberOfItemsInRow:(NSInteger)row inColumn:(NSInteger)column {
    if (column == 0) {
        if (row == 3) {
            return self.jiachang.count;
        }else if (row == 4) {
            return self.difang.count;
        }else if (row == 5) {
            return self.tese.count;
        }else if (row == 6) {
            return self.rihan.count;
        }else if (row == 7) {
            return self.xishi.count;
        }else if (row == 8) {
            return self.shaokao.count;
        }
    }
    return 0;
}

- (NSString *)menu:(LrdSuperMenu *)menu titleForItemsInRowAtIndexPath:(LrdIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (indexPath.column == 0) {
        if (row == 3) {
            return self.jiachang[indexPath.item];
        }else if (row == 4) {
            return self.tese[indexPath.item];
        }else if (row == 5) {
            return self.rihan[indexPath.item];
        }else if (row == 6) {
            return self.xishi[indexPath.item];
        }else if (row == 7) {
            return self.shaokao[indexPath.item];
        }
    }
    return nil;
}

- (void)menu:(LrdSuperMenu *)menu didSelectRowAtIndexPath:(LrdIndexPath *)indexPath {
    if (indexPath.item >= 0) {
        NSLog(@"点击了 %ld - %ld - %ld 项目",indexPath.column,indexPath.row,indexPath.item);
    }else {
        NSLog(@"点击了 %ld - %ld 项目",indexPath.column,indexPath.row);
    }
}

//点击键盘上的search按钮时调用

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar

{
    
    NSString *searchTerm = searchBar.text;
    
    [self removeSearchViewFromSuper];
    
    NSLog(@"SEARCH:%@",searchTerm);
    
    //[self handleSearchForTerm:searchTerm];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
