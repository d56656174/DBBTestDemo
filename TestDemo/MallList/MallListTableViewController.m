//
//  MallListTableViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/14.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallListTableViewController.h"
#import "MallListTableViewCell.h"
#import "Product.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MallListTableViewController ()


@end

static NSString *CellIdentifierTab = @"IndexTab";

@implementation MallListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.tableView.dataSource = self;
    //self.tableView.delegate = self;
    //[self.tableView registerClass:[MallListTableViewCell class] forCellReuseIdentifier:CellIdentifierTab];
    

    self.dataArray1 = [[NSMutableArray alloc] init];
    self.imageArray1 = [[NSMutableArray alloc] initWithArray:@[@"http://ww1.sinaimg.cn/mw600/005vbOHfgw1f3v9kejyamj30b10gojt4.jpg",@"http://ww2.sinaimg.cn/mw600/81f937cegw1f3v2y0yx0fj20t60t6dp4.jpg",@"http://ww3.sinaimg.cn/mw600/005vbOHfgw1f3u02y5u92j31kw2dch46.jpg",@"http://ww4.sinaimg.cn/mw600/005vbOHfgw1f3u029k849j30ia0ia76g.jpg",@"http://ww2.sinaimg.cn/mw600/005vbOHfgw1f3u01vair9j30ev0jlq4y.jpg",@"http://ww4.sinaimg.cn/mw600/67175fe4jw1f3top558zvj20es0m8dka.jpg",@"http://ww1.sinaimg.cn/mw600/6cca1403jw1f3ss7n24drj20gc0ext9n.jpg",@"http://ww1.sinaimg.cn/mw600/aa594a48gw1f3v6r0ndn8j20zk0npn71.jpg",@"http://ww4.sinaimg.cn/mw600/6cca1403jw1f3sswod21wj20gh0fxdgf.jpg",@"http://ww3.sinaimg.cn/mw600/6cca1403jw1f3suddbmdmj20ev0g2dgk.jpg",@"http://ww4.sinaimg.cn/mw600/6cca1403jw1f3sukzia17j20ex0g6dgn.jpg",@"http://ww1.sinaimg.cn/mw600/005vbOHfgw1f3sskyorsxj30k00qo76k.jpg"]];
    
    for(int i=0;i<10;i++)
    {
        Product* p1 = [[Product alloc] init];
        [p1 setName:@"1332222323232323"];
        [p1 setId:i];
        [p1 setPrice1:@"123.123"];
        [p1 setPrice2:[NSString stringWithFormat:@"运费 %@", @"2233.11" ]];
        [p1 setPortraitURL:[NSURL URLWithString:[self.imageArray1 objectAtIndex:i]]];
        [p1 setSalecount:@"当月销量"];
        [p1 setProductat:@"北京"];
        [self.dataArray1 addObject:p1];
    }
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"搜索"
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(searchKeyWord)];
    //[self.tableView reloadData]
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return [self.dataArray1 count];
    //return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0)
    {
        return 0;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Product* p = [self.dataArray1 objectAtIndex:indexPath.section];
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierBanner forIndexPath:indexPath];
    
    // Configure the cell...
//    MallListTableViewCell *cell = (MallListTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierTab];
//    if(cell == nil)
//    {
        //reuseIdentifier 重用cell
         MallListTableViewCell *cell = [[MallListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierTab];
        //[cell initSubviews];
        cell.nameLabel.text = p.name;
        cell.priceLabel1.text = p.price1;
        cell.priceLabel2.text = p.price2;
        cell.salecountLabel.text = p.salecount;
        cell.productatLabel.text = p.productat;
        //cell..text = p.portraitURL;

        

    //}
    UIImage *image = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:p.portraitURL.absoluteString];
    // 有图就加载，无图则下载并reload tableview
    if (!image) {
        //[cell.image setImage:[UIImage imageNamed:@"loading"]];
        [self downloadThumbnailImageThenReload:p.portraitURL];
    } else {
        [cell.image setImage:image];
    }

    return cell;

   
}

- (void)downloadThumbnailImageThenReload:(NSURL*)url
{
    [SDWebImageDownloader.sharedDownloader downloadImageWithURL:url
                                                        options:SDWebImageDownloaderUseNSURLCache
                                                       progress:nil
                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                          [[SDImageCache sharedImageCache] storeImage:image forKey:url.absoluteString toDisk:NO];
                                                          
                                                          // 单独刷新某一行会有闪烁，全部reload反而较为顺畅
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              NSLog(@"%@",url);
                                                              //NSAssert(self.tableView == nil, @"Debugging test");
                                                              NSLog(@"%@",self.tableView);
                                                             [self.tableView reloadData];
                                                              
                                                          });
                                                      }];
    
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
