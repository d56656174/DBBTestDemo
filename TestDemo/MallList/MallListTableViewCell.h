//
//  MallListTableViewCell.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/14.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallListTableViewCell : UITableViewCell
@property (strong,nonatomic)UILabel* nameLabel;
@property (strong,nonatomic)UILabel* priceLabel1;
@property (strong,nonatomic)UILabel* priceLabel2;
@property (strong,nonatomic)UIImageView* image;
@property (strong,nonatomic)UILabel* salecountLabel;
@property (strong,nonatomic)UILabel* productatLabel;
-(void)initSubviews;
-(void)setLayout;
@end
