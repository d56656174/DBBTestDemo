//
//  MaillListViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/15.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallListTableViewController.h"
@interface MaillListViewController : UIViewController
@property(strong,nonatomic) MallListTableViewController *mallListTable;

@end
