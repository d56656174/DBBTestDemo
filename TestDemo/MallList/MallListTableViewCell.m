//
//  MallListTableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/14.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallListTableViewCell.h"

@implementation MallListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
        [self initSubviews];
        [self setLayout];
    }
    return self;
}
-(void)initSubviews
{
    [self setImage:[[UIImageView alloc] init]];
    [self setNameLabel:[[UILabel alloc] init]];
    [self setPriceLabel1:[[UILabel alloc] init]];
    [self setPriceLabel2:[[UILabel alloc] init]];
    [self setProductatLabel:[[UILabel alloc] init]];
    [self setSalecountLabel:[[UILabel alloc] init]];
    self.priceLabel1.font = [UIFont systemFontOfSize:12];
    self.priceLabel2.font = [UIFont systemFontOfSize:12];
    self.productatLabel.font = [UIFont systemFontOfSize:12];
    self.salecountLabel.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.image];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.priceLabel1];
    [self.contentView addSubview:self.priceLabel2];
    [self.contentView addSubview:self.productatLabel];
    [self.contentView addSubview:self.salecountLabel];
}


-(void)setLayout
{
    for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
    NSDictionary *superviewdict = @{@"nameLabel":_nameLabel,@"priceLabel1":_priceLabel1,@"priceLabel2":_priceLabel2,@"image":_image,@"salecountLabel":_salecountLabel,@"productatLabel":_productatLabel};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[image]-|"] options:0 metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[image(80)]-20-[nameLabel]-8-|"] options:0 metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[image(80)]-20-[priceLabel1]-[priceLabel2]"] options:0 metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-[image(80)]-20-[salecountLabel]-[productatLabel]"] options:NSLayoutFormatAlignAllBottom metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[nameLabel]-20-[priceLabel1]-[salecountLabel]"] options:NSLayoutFormatAlignAllLeft metrics:nil views:superviewdict]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[priceLabel1]-[priceLabel2]"] options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[salecountLabel]-[productatLabel]"] options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];
//    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[salecountLabel][productatLabel]"] options:NSLayoutFormatAlignAllCenterY metrics:nil views:superviewdict]];

}
@end
