//
//  TESTDLeftViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/3/14.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "TESTDLeftViewController.h"

@interface TESTDLeftViewController ()

@end

@implementation TESTDLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(150.0, 120.0, 200.0, 50.0)];
    label1.text = @"资讯";
    [self.view addSubview:label1];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
