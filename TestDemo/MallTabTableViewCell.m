//
//  MallTabTableViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallTabTableViewCell.h"
#import "MaillTabIconCollectionViewController.h"

@interface MallTabTableViewCell()
@property(nonatomic,strong) MaillTabIconCollectionViewController *collection;
@end

@implementation MallTabTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
    }
    return self;
}

-(void)initSubviews
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(60, 80);
    // 3.设置整个collectionView的内边距
    CGFloat paddingY = 25;
    CGFloat paddingX = 25;
    layout.sectionInset = UIEdgeInsetsMake(5, paddingX, paddingY, paddingX);
    // 4.设置每一行之间的间距
    layout.minimumLineSpacing = 30;
    
    [self setCollection: [[MaillTabIconCollectionViewController alloc] initWithCollectionViewLayout:layout]];
    
    self.collection.collectionView.backgroundColor = self.contentView.backgroundColor;
    self.collection.collectionView.frame = self.contentView.frame;

    [self.collection setDataArray1:self.dataArray1];
    //[self.collection loadView];
    [self.collection.collectionView reloadData];
    [self.contentView addSubview:self.collection.view];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
