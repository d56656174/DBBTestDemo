//
//  MallTabIconCollectionViewCell.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallTabIconCollectionViewCell.h"

@implementation MallTabIconCollectionViewCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //        self.backgroundColor = [UIColor themeColor];
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        //        UIView *selectedBackground = [UIView new];
        //        selectedBackground.backgroundColor = [UIColor colorWithHex:0xF5FFFA];
        //        [self setSelectedBackgroundView:selectedBackground];
        //self.tintColor = [UIColor colorWithHex:0x15A230];
        
        //[self setLayout];
    }
    return self;
}

-(void)initSubviews:(UIImage*)image labeltxt:(NSString*)lab
{
    self.icon = [[UIImageView alloc] init];
    self.icon.image = image;
    [self.contentView addSubview:self.icon];
    self.label = [[UILabel alloc] init];
    self.label.text = lab;
    self.label.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.label];
    
    [self setLayout];
}

-(void)setLayout
{
        for (UIView *view in self.contentView.subviews) {view.translatesAutoresizingMaskIntoConstraints = NO;}
    
        NSDictionary *superviewdict = @{@"_pic":self.icon,@"supperview":self.contentView,@"label":self.label};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[_pic(%f)]",self.contentView.frame.size.width] options:0 metrics:nil views:superviewdict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[label(%f)]",self.contentView.frame.size.width] options:0 metrics:nil views:superviewdict]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-[_pic(%f)]-[label(%f)]",self.contentView.frame.size.height*0.75,self.contentView.frame.size.height*0.25] options:0 metrics:nil views:superviewdict]];
}


@end
