//
//  TESTPerson+Test.h
//  TestDemo
//
//  Created by 董冰彬 on 16/2/5.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TESTPerson.h"

@interface TESTPerson(TEST)
-(void)sayHello;
@end
