//
//  Product.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/14.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property (nonatomic, assign) int id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *price1;
@property (nonatomic, copy) NSString *price2;
@property (nonatomic, strong) NSURL *portraitURL;
@property (nonatomic, strong) NSString *salecount;
@property (nonatomic, strong) NSString *productat;
@end
