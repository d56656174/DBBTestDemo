//
//  TableBarViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/3/6.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "TableBarViewController.h"
#import "SwipableViewController.h"
#import "ViewController1.h"
#import "ViewController2.h"
#import "ViewController3.h"

@interface TableBarViewController ()<UITabBarControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation TableBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
    
    
    ViewController1 *view1 = [[ViewController1 alloc] init];
    view1.view.backgroundColor = [UIColor blueColor];
    ViewController2 *view2 = [[ViewController2 alloc] init];
    view2.view.backgroundColor = [UIColor redColor];
    ViewController3 *view3 = [[ViewController3 alloc] init];
    view3.view.backgroundColor = [UIColor greenColor];
    ViewController3 *view4 = [[ViewController3 alloc] init];
    view4.view.backgroundColor = [UIColor brownColor];
    SwipableViewController *newsSVC = [[SwipableViewController alloc] initWithTitle:@"综合"
                                                                       andSubTitles:@[@"资讯", @"热点", @"博客",@"博客1"]
                                                                     andControllers:@[view1, view2, view3,view4]
                                                                        underTabbar:YES];
    self.tabBar.translucent = NO;
    self.viewControllers = @[
                             [self addNavigationItemForViewController:newsSVC]
                             ];
}

#pragma mark -

- (UINavigationController *)addNavigationItemForViewController:(UIViewController *)viewController
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    viewController.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigationbar-sidebar"]
                                                                                        style:UIBarButtonItemStylePlain
                                                                                       target:self action:@selector(onClickMenuButton)];
    
    viewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                                     target:self
                                                                                                     action:@selector(pushSearchViewController)];
    
    
    
    return navigationController;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
