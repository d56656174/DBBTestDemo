//
//  MallIndexTableViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallIndexTableViewController : UITableViewController
@property (strong,nonatomic) NSMutableArray *dataArray1;
@property (strong,nonatomic) NSMutableArray *dataArray2;
@end
