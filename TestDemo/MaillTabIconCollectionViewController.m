//
//  MaillTabIconCollectionViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MaillTabIconCollectionViewController.h"
#import "MallTabIconCollectionViewCell.h"

@interface MaillTabIconCollectionViewController ()

@end

@implementation MaillTabIconCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[MallTabIconCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    //self.collectionViewLayout = layout;
    //[self setDataArray1:darr];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    
    return [self.dataArray1 count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MallTabIconCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
//    if(indexPath.section==0)
//    {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", @(indexPath.row +1)]];
        [cell initSubviews:image labeltxt:[NSString stringWithFormat:@"%@", @(indexPath.row +1)]];
        //return cell;
//    }
//    else if(indexPath.section==1)
//    {
//        
//    }
//    else if(indexPath.section==2)
//    {
//        
//    }
//    else if(indexPath.section==3)
//    {
//        
//    }
//    else if(indexPath.section==4)
//    {
//        
//    }
//    else if(indexPath.section==5)
//    {
//        
//    }
//    else if(indexPath.section==6)
//    {
//        
    //}
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
