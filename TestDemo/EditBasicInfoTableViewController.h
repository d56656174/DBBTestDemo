//
//  EditBasicInfoTableViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 16/4/26.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPickerView.h"
@interface EditBasicInfoTableViewController : UITableViewController

@property (strong,nonatomic) NSMutableArray *dataArray1;
@property (strong,nonatomic) NSMutableArray *valueArray1;

@property (strong,nonatomic) NSMutableArray *dataArray2;
@property (strong,nonatomic) NSMutableArray *valueArray2;


@end
