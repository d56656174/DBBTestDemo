//
//  ViewController3.m
//  TestDemo
//
//  Created by 董冰彬 on 16/3/6.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "ViewController3.h"

@interface ViewController3 ()

@end

@implementation ViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 20.0, 200.0, 50.0)];
    label1.text = @"博客";
    [self.view addSubview:label1];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
