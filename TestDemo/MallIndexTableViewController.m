//
//  MallIndexTableViewController.m
//  TestDemo
//
//  Created by 董冰彬 on 16/5/8.
//  Copyright © 2016年 dongbingbin. All rights reserved.
//

#import "MallIndexTableViewController.h"
#import "RollingBannerTableViewCell.h"
#import "MallTabTableViewCell.h"

@interface MallIndexTableViewController ()

@end

@implementation MallIndexTableViewController

static NSString *CellIdentifierBanner = @"IndexBanner";
static NSString *CellIdentifierTab = @"IndexTab";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0)
    {
        return 0;
    }
    else
    {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        return 200;
    }
    else
    {
        return 250;
    }
    
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    if(section==0)
    {
        return 1;
    }
    else if(section==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierBanner forIndexPath:indexPath];
    
    // Configure the cell...
    
    if(indexPath.section==0)
    {
        RollingBannerTableViewCell *cell = (RollingBannerTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierBanner];
        if(cell == nil)
        {
            
            cell = [[RollingBannerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBanner];
            NSMutableArray *darr = [[NSMutableArray alloc] initWithArray:@[@"http://easyread.ph.126.net/G8GtEi-zmPQzvS5w7ScxmQ==/7806606224489671909.jpg"
                                                                         , @"https://c2.staticflickr.com/4/3345/5832660048_55f8b0935b.jpg"
                                                                         , @"http://epaper.syd.com.cn/sywb/res/1/20080108/42241199752656275.jpg"
                                                                           ]];
            [cell setDataArray1:darr];
            [cell initSubviews];
        }
        

        return cell;
        
    }
    else if(indexPath.section==1)
    {
        MallTabTableViewCell *cell = (MallTabTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifierTab];
        if(cell == nil)
        {
            
            cell = [[MallTabTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierBanner];
            

            NSMutableArray *darr = [[NSMutableArray alloc] initWithArray:@[@"1"
                                                                           , @"2"
                                                                           , @"3", @"4", @"5", @"6", @"7", @"8"
                                                                           ]];
            [cell setDataArray1:darr];
            [cell initSubviews];
        }
        
        return cell;
    }
    else
    {
        return nil;
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
