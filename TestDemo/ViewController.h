//
//  ViewController.h
//  TestDemo
//
//  Created by 董冰彬 on 15/11/20.
//  Copyright © 2015年 dongbingbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong,nonatomic)UIScrollView* scrollview;


@property (strong,nonatomic)UIImageView* phoneLabel;

@property (strong,nonatomic)UILabel* pwdLabel;

@property (strong,nonatomic)UITextField* phonetxt;

@property (strong,nonatomic)UITextField* pwdtxt;

@property (strong,nonatomic)UILabel* fogetpwdLabel;

@property (strong,nonatomic)UIButton* signinBtn;

@property (strong,nonatomic)UIButton* signupBtn;

@property (strong,nonatomic)UIView* lineView1;

@property (strong,nonatomic)UIView* lineView2;

@property (strong,nonatomic)UIView* boxview;

@end

